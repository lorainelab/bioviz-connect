class CoverageGraphAnalysisRequest:
    def __init__(self):
        self.name = ""
        self.app_id = ""
        self.system_id = "de"
        self.debug = False
        self.create_output_subdir = False
        self.archive_logs = False
        self.output_dir = ""
        self.notify = True
        self.config = {}
