class SortRequest:
    def __init__(self, sort_order, sort_field):
        self.field = sort_field
        self.order = sort_order
