from .Sharing import Sharing
from .Path import Path
from .CustomEncoder import CustomEncoder
from .ShareRequest import ShareRequest
from .UnshareRequest import UnshareRequest
from .CoverageGraphAnalysisRequest import CoverageGraphAnalysisRequest
from .SearchFileAllRequest import SearchFileAllRequest
from .SearchFileArgsRequest import SearchFileArgsRequest
from .SearchFileExactRequest import SearchFileExactRequest
from .SearchFileModelRequest import SearchFileModelRequest
from .SearchFileOwnerRequest import SearchFileOwnerRequest
from .SearchFileSectionRequest import SearchFileSectionRequest
from .. import settings
from .SortRequest import SortRequest
from .FilePermissionRequest import FilePermissionRequest
from .EncoderPaths import EncoderPaths
from .. import models
from .PathtoCreateFolder import PathtoCreateFolder
from urllib.parse import unquote_plus
from .RenameFileFolder import RenameFileFolder
from .MoveFile import MoveFile
from .UploadFileByURL import UploadFileByURL
from .DeleteFile import DeleteFiles
class RequestCreator:
    @staticmethod
    def create_share_folder_request(filepath, user, permission):
        share_data = Sharing()
        share_data.user = user
        p = Path()
        p.path = filepath
        p.permission = permission
        share_data.paths.append(p)
        share_req = ShareRequest()
        share_req.sharing.append(share_data)
        custom_encoder = CustomEncoder()
        return custom_encoder.encode(share_req)

    @staticmethod
    def create_unshare_request(filepaths, user):
        unshare_data = Sharing()
        unshare_data.user = user
        path_data = filepaths.split(',')
        for path in path_data:
            if path is not None:
                unshare_data.paths.append(path)
        unshare_req = UnshareRequest()
        unshare_req.unshare.append(unshare_data)
        custom_encoder = CustomEncoder()
        return custom_encoder.encode(unshare_req)

    @staticmethod
    def create_coveragegraph_request(name, appid, systemid, debug, createOutputDir, archiveLogs, outputdirpath, notify, config):

        requestdata = CoverageGraphAnalysisRequest()
        requestdata.name = name
        requestdata.app_id = appid
        requestdata.system_id = systemid
        requestdata.debug = debug
        requestdata.create_output_subdir = createOutputDir
        requestdata.archive_logs = archiveLogs
        requestdata.output_dir = outputdirpath
        requestdata.notify = notify
        requestdata.config = config
        custom_encoder = CustomEncoder()
        return custom_encoder.encode(requestdata)

    @staticmethod
    def create_Scalecoveragegraph_request(name, appid, systemid, debug, createOutputDir, archiveLogs, outputdirpath,
                                  notify, inputfilepath, outputfilename,config_input_id,config_output_id, id_input_cpm, id_val_cpm, id_outputtype, id_outputtypevalue, input_Index_FilePath, config_Index_input_id):

        requestdata = CoverageGraphAnalysisRequest()
        requestdata.name = name
        requestdata.app_id = appid
        requestdata.system_id = systemid
        requestdata.debug = debug
        requestdata.create_output_subdir = createOutputDir
        requestdata.archive_logs = archiveLogs
        requestdata.output_dir = outputdirpath
        requestdata.notify = notify
        config = dict()
        config[config_input_id] = inputfilepath
        config[config_Index_input_id] = input_Index_FilePath

        ## get the cpm attributes
        cpm = models.Arguments.objects.filter(id=id_val_cpm)[0]
        cpm_dict = dict()
        cpm_dict['name'] = cpm.Name
        cpm_dict['isDefault'] = bool(cpm.isDefault)
        cpm_dict['id'] = id_val_cpm
        cpm_dict['display'] = cpm.Display
        cpm_dict['value'] = cpm.value
        config[id_input_cpm] = cpm_dict

        ## get the output type attributes
        output = models.Arguments.objects.filter(id=id_outputtypevalue)[0]
        output_dict = dict()
        output_dict['name'] = output.Name
        output_dict['isDefault'] = bool(output.isDefault)
        output_dict['id'] = id_outputtypevalue
        output_dict['display'] = output.Display
        output_dict['value'] = output.value
        config[id_outputtype] = output_dict

        config[config_output_id] = outputfilename+'.'+output.value
        requestdata.config = config
        custom_encoder = CustomEncoder()
        return custom_encoder.encode(requestdata)

    @staticmethod
    def generate_userownedSectionRequest(owner):
        args_Search_section = SearchFileArgsRequest()
        args_Search_section.type = "owner"
        args_Search_section.args = SearchFileOwnerRequest(owner)
        return args_Search_section

    @staticmethod
    def generate_homeSectionRequest(owner, home_path):
        args_Search_section = SearchFileArgsRequest()
        args_Search_section.type = "path"
        base_homepath = home_path + owner
        args_Search_section.args = SearchFileSectionRequest(base_homepath)
        return args_Search_section

    @staticmethod
    def generate_communitySectionRequest(community_path):
        args_Search_section = SearchFileArgsRequest()
        args_Search_section.type = "path"
        base_community = community_path
        args_Search_section.args = SearchFileSectionRequest(base_community)
        return args_Search_section

    @staticmethod
    def generate_sharedSectionRequest(owner, home_path):
        return RequestCreator.generate_homeSectionRequest("", home_path)



    @staticmethod
    def create_search_typelabel_request(exact, label, owner, type,  home_path, community_path):

        list_ofall = []

        args_search_label = SearchFileArgsRequest()
        args_search_label.type = "label"
        exact_Search = SearchFileExactRequest()
        exact_Search.exact = exact
        exact_Search.label = label
        args_search_label.args = exact_Search
        list_ofall.append(args_search_label)


        if type.lower() == "owner":
            list_ofall.append(RequestCreator.generate_userownedSectionRequest(owner))
        elif type.lower() == "home":
            list_ofall.append(RequestCreator.generate_homeSectionRequest(owner, home_path))
        elif type.lower() == "community":
            list_ofall.append(RequestCreator.generate_communitySectionRequest(community_path))
        elif type.lower() == "shared":
            list_ofall.append(RequestCreator.generate_sharedSectionRequest(owner, home_path))
        return list_ofall



    @staticmethod
    def create_search_all_request(exact, label, username, type, home_path, community_path):
        allrequest  = SearchFileAllRequest()
        allrequest.all = RequestCreator.create_search_typelabel_request(exact, label, username, type, home_path, community_path)
        if type.lower() == "shared":
            allrequest.none.append(RequestCreator.generate_communitySectionRequest(community_path))
            allrequest.none.append(RequestCreator.generate_homeSectionRequest(username, home_path))
        return allrequest

    @staticmethod
    def create_searchFile_request(exact, label, username, size, type, offset, sort_field, sort_order, home_path, community_path):
        # initiate objects
        requestdata = SearchFileModelRequest(size, offset, sort_order, sort_field)

        requestdata.query = RequestCreator.create_search_all_request(exact, label, username, type, home_path, community_path)
        requestdata.size = size
        requestdata._from = offset

        custom_encoder = CustomEncoder()
        request_json = custom_encoder.encode(requestdata)
        request_json = request_json.replace('_from', 'from')
        return request_json


    @staticmethod
    def create_search_checkpermission_request(label, size, offset):
        label_name = label.split('/')[-1]

        requestdata = SearchFileModelRequest(size, offset, 'descending', 'dateCreated')
        allrequest  = SearchFileAllRequest()

        list_ofall = []

        args_search_label = SearchFileArgsRequest()
        args_search_label.type = "label"
        exact_Search = SearchFileExactRequest()
        exact_Search.exact = True
        exact_Search.label = label_name
        args_search_label.args = exact_Search
        list_ofall.append(args_search_label)
        list_ofall.append(RequestCreator.generate_communitySectionRequest(label))
        allrequest.all = list_ofall

        requestdata.query = allrequest
        requestdata.size = size
        requestdata.__from = offset

        custom_encoder = CustomEncoder()
        request_json = custom_encoder.encode(requestdata)
        new_json = request_json.replace('_from', 'from')
        if '_RequestCreator_from' in new_json:
                start_index = new_json.find('_RequestCreator_from')
                string_trim = '"_RequestCreator_from": 0'
                new_json = new_json[:start_index-3] + new_json[start_index + len(string_trim)-1:]
        return new_json

    @staticmethod
    def create_config(config_data):
        config_request = []
        if config_data is not None:
            for data in config_data:
                config_request[data] = config_data[data]
        return config_data

    @staticmethod
    def create_file_permission_request(filepaths):
        filepermisison_request = FilePermissionRequest()
        path_data = []
        paths = filepaths.split(',')
        for path in paths:
            if path is not None:
                path_data.append(path)
        filepermisison_request.paths = paths
        custom_encoder = CustomEncoder()
        return custom_encoder.encode(filepermisison_request)

    @staticmethod
    def getFileId(filepath):
        paths_list = EncoderPaths()
        paths_list.paths.append(filepath)
        custom_encoder = CustomEncoder()
        return custom_encoder.encode(paths_list)

    @staticmethod
    def createFolderRequest(filepath):
        path = PathtoCreateFolder()
        path.path = unquote_plus(filepath)
        custom_encoder = CustomEncoder()
        return custom_encoder.encode(path)

    @staticmethod
    def renameFileFolder_request(source, dest):
        rename = RenameFileFolder()
        rename.source = unquote_plus(source)
        rename.dest = unquote_plus(dest)
        custom_encoder = CustomEncoder()
        return custom_encoder.encode(rename)

    @staticmethod
    def moveFileFolder_request(source, dest):
        movefile = MoveFile()
        movefile.sources = source
        movefile.dest = dest
        custom_encoder = CustomEncoder()
        return custom_encoder.encode(movefile)

    @staticmethod
    def uploadFileByURL_request(address, dest):
        urlFile = UploadFileByURL()
        urlFile.address = address
        urlFile.dest = dest
        custom_encoder = CustomEncoder()
        return custom_encoder.encode(urlFile)

    @staticmethod
    def deleteFileFolder_request(paths):
        deleteFiles = DeleteFiles()
        deleteFiles.paths = paths
        custom_encoder = CustomEncoder()
        return custom_encoder.encode(deleteFiles)
