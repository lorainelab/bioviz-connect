# share file
import requests
from .. import settings
from ..terrain_model.ResponseParser import ResponseParser
from ..terrain_model.RequestCreator import RequestCreator
from ..terrain_model.CustomEncoder import CustomEncoder


def shareFile(filepath, user, accesstoken):
    try:
        permission = "read"
        request_data = RequestCreator.create_share_folder_request(filepath, user, permission)
        req_url = 'https://de.cyverse.org/terrain/secured/share'
        r = requests.post(req_url, headers={'Authorization': 'BEARER ' + accesstoken,
                                       'Content-type': 'application/json'},
                      data=request_data)

        json_data = r.json()
        if 'error_code' in json_data:
            if (json_data['error_code'] == 'ERR_NOT_AUTHORIZED'):
                raise PermissionError()

        share_response = ResponseParser.share_response(r.json())
        custom_encoder = CustomEncoder()
        res = custom_encoder.encode(share_response)
        return res
    except PermissionError as error:
        raise PermissionError()




