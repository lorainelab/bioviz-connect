# run coverage graph analysis
import requests
from django.http import HttpResponseBadRequest
from .. import settings
from ..terrain_model.RequestCreator import RequestCreator
from ..terrain_model.ResponseParser import ResponseParser
from ..terrain_model.CustomEncoder import CustomEncoder

def runRestCoverageGraphAnalysis(name, appid, systemid, debug, createOutputDir, archiveLogs, outputdirpath, notify, config, accesstoken):
    try:
        request = RequestCreator.create_coveragegraph_request(name, appid, systemid, debug, createOutputDir, archiveLogs, outputdirpath, notify, config)
        req_url = 'https://de.cyverse.org/terrain/analyses'
        r = requests.post(req_url, headers={'Authorization': 'BEARER ' + accesstoken,
                                        'Content-Type': 'application/json'},
                        data=request)

        json_data = r.json()
        if 'error_code' in json_data:
            if (json_data['error_code'] == 'ERR_NOT_AUTHORIZED'):
                raise PermissionError()
            if (json_data['error_code'] == 'ERR_DOES_NOT_EXIST'):
                raise FileNotFoundError()

        response = ResponseParser.parse_coveragegraph_analysis_response(r.json())
        custom_encoder = CustomEncoder()
        res = custom_encoder.encode(response)
        return res
    except PermissionError as error:
        raise PermissionError()
    except FileNotFoundError as error:
        raise FileNotFoundError()

