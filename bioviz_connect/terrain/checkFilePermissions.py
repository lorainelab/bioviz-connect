# check file permissions
import requests
from django.core.exceptions import PermissionDenied

from .. import settings
from ..terrain_model.RequestCreator import RequestCreator
from ..terrain_model.ResponseParser import ResponseParser
from ..terrain_model.CustomEncoder import CustomEncoder


def checkFilePermissions(filepath, redis_sessiondata):
    try:
        # Multiple filepaths can be send separated by ','
        accesstoken = str(redis_sessiondata[0])
        request = RequestCreator.create_file_permission_request(filepath)
        req_url = 'https://de.cyverse.org/terrain/secured/filesystem/user-permissions'
        r = requests.post(req_url, headers={'Authorization': 'BEARER ' + accesstoken,
                                        'Content-type': 'application/json'},
                      data=request)
        json_data = r.json()
        if 'error_code' in json_data:
            print(json_data['error_code'])
            if (json_data['error_code'] == 'ERR_NOT_AUTHORIZED'):
                raise PermissionError()
            if (json_data['error_code'] == 'ERR_NOT_OWNER'):
                raise Exception()

        response = ResponseParser.parse_file_permission_response(r.json())
        custom_encoder = CustomEncoder()
        res = custom_encoder.encode(response)
        return res
    except PermissionError as error:
        raise PermissionError()
    except Exception as error:
        return json_data['error_code']


def checkFilePermissions_search(filepath, redis_sessiondata):
    try:
        # Multiple filepaths can be send separated by ','
        accesstoken = str(redis_sessiondata[0])
        request = RequestCreator.create_search_checkpermission_request(filepath, 1, 0)
        req_url = 'https://de.cyverse.org/terrain/secured/filesystem/search'
        r = requests.post(req_url, headers={'Authorization': 'BEARER ' + accesstoken,
                                        'Connection': 'keep-alive',
                                        'Content-Type': 'application/json'},
                                        data= request)
        json_data = r.json()
        if 'error_code' in json_data:
            if (json_data['error_code'] == 'ERR_NOT_AUTHORIZED'):
                raise PermissionError()
            if (json_data['error_code'] == 'ERR_NOT_OWNER'):
                raise Exception()

        response = ResponseParser.parse_search_isPublic_response(json_data)

        custom_encoder = CustomEncoder()
        res = custom_encoder.encode(response)
        if 'user_permissions' in res:
            return res.replace('user_permissions', 'user-permissions')
        return res
    except PermissionError as error:
        raise PermissionError()
    except Exception as error:
        return Exception()
