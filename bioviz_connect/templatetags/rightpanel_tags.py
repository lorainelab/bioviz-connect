from django import template

from bioviz_connect.terrain.ManageAppDetails import getAppDataWithAppId
from .. import models
register = template.Library()

@register.inclusion_tag('rightpanel.html')
def rightpanel_render():
    return


@register.inclusion_tag('rp_metadata.html')
def metadata_render():
    return

@register.inclusion_tag('rp_managelink.html')
def managelink_render():
    return


@register.inclusion_tag('rp_analysis.html')
def analysis_render():
    return

@register.inclusion_tag('partials/_chosen_app_template.html')
def app_analysis_render():
    appData = {}
    appId = "91c41516-8c89-11e9-890e-008cfa5ae621"
    inputfields = models.InputDetails.objects.filter().all()
    for num, field in enumerate(inputfields, start=0):
        if field.AppId.__str__() in appData.keys():
            appData[field.AppId.__str__()].append({'id': field.id, 'type': field.Formtype, 'description': field.Description,
                                          'label': field.label, 'inputType': field.Inputtype,
                                          'formType': field.Formtype});
        else:
            appData[field.AppId.__str__()] = [{'id': field.id, 'type': field.Formtype, 'description': field.Description,
                                     'label': field.label, 'inputType': field.Inputtype, 'formType': field.Formtype}];

    return {"appList":appData}

