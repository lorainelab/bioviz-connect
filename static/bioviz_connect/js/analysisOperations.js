var analyseData;
var fileDir;
function analyseUI(path, format){
fileDir=getInputPath(path);
$.ajax({
        method: "GET",
        dataType: "json",
        async: "true",
		url: location.origin + '/getAppsByFileFormat/?Format='+format,
	    success: function (data, status, jqXHR) {
            loadingDisplay(".rightLoad",false);
            analyseOps(data);
        },
        error: function (jqXHR, status, err) {
            alert("Sorry! Unable to generate Analysis form. Please try again or contact admin.")
	    },
            complete: function (jqXHR, status) {
        }
	});
}

function trimAppDescription(description){
    if (description.includes('%INPUT%')){
        return description.slice(0, description.indexOf('%INPUT%'));
    }else{
        return description
    }
}

function analyseOps(data){
    $(".accord").remove();
    for(var i=0;i<data.length;i++){
        var acco= "<div class='list-group-item accord'>"
            acco+="<div class='container'>"
            acco+="<button type='button' class='btn btn-primary text-white w-100' onclick='formUI(\""+data[i].id+"\",\""+data[i].Name+"\")'>"+data[i].Name+"</button>"
            acco+="<button class='btn  w-100 ' data-toggle='collapse' data-target='#demo"+i+"'><i class='fas fa-angle-down'></i></button>"
            acco+= "<div id='demo"+i+"' class='collapse wrapword text-secondary'>"+trimAppDescription(data[i].description)+"</div></div></div>"
            $("#dynamicAnalyse").append(acco)
    }
}
function formUI(appID,appName){
    $("#analysis").css("display","none");
    $("#appForm").data("appID",appID);
    $("#appForm").children(".container").children("h5").html(appName)
    $("#appForm").css("display","block");
    $("#theForm").children().css("display","none");
    $("#"+appID).css("display","block");
    $(".analysisName").val("");
    var elementList = eval($("#"+appID).attr("data-analyse"));
    elementList.forEach(function(ele){
        $("#"+ele.id).val("");
        if(ele.type.toString() == "FileInput"){
            if(ele.label.toString().indexOf("Index")<=0){
                var length =$(".right").data("fileInfo").path.split("/").length
                var fileName = $(".right").data("fileInfo").path.split("/")[length-1]
                $("#"+ele.id).val(fileName)
            }
        }
    });
}
function formRunData(appID,elementList){
    var status=true ;
    var form_element_value;
    var fd = new FormData();
    var element_fd = new FormData();
    fd.append('name', $(".analysisName").val())
    fd.append('AppId',$('#appForm').data("appID"))
    fd.append('outputdir',fileDir)
    elementList.forEach(function(ele){
    $("#"+ele.id).removeClass("is-invalid")
    $(".analysisName").removeClass("is-invalid");
    if($("#analyse_name_"+appID).val()==""){
        status = false;
        $("#analyse_name_"+appID).addClass("is-invalid");
    }
    switch(ele.type.toString()){
        case "TextSelection":
            form_element_value=$("#"+ele.id).text();
            if(form_element_value==""){
                status = false;
                $('.analysisInputError').toast('show');
            }
            break;
        case "FileInput":

            var form_element_value = $("#"+ele.id).val();
            if(form_element_value==""){
                 status = false;
                $("#"+ele.id).addClass("is-invalid");
                $('.analysisInputError').toast('show');
            }
            form_element_value= fileDir+"/"+form_element_value
            break;
        case "FileOutput":
            var form_element_value = $("#"+ele.id).val();
            if(form_element_value==""){
                 status = false;
                $("#"+ele.id).addClass("is-invalid");
                $('.analysisInputError').toast('show');
            }
            break;
        case "Integer":
            form_element_value= $("#"+ele.id).val();
            if(form_element_value==""){
                status = false;
                $("#"+ele.id).addClass("is-invalid");
                $('.analysisInputError').toast('show');
            }
            break;
        case "FolderOutput":
            form_element_value= $("#"+ele.id).val();
            if(form_element_value==""){
                 status = false;
                $("#"+ele.id).addClass("is-invalid");
                $('.analysisInputError').toast('show');
                break;
            }
            break;
        default:
            status = false;
            $("#"+ele.id).parent().html("<div class='alert alert-danger' role='alert'>Server Error! Please report this to the admin.</div>")
            break;
        }
        if(status)
            element_fd.append(ele.id,form_element_value);
    });
    if(status){
        runAnalysis(fd,element_fd,appID)
    }
}
function getInputPath(path){
    path = path.substring(0,path.lastIndexOf("/")+1);
    return path;
}
function runAnalysis(fd,element_fd,appID){
    currentsection = $("#page-selection").data("currentSection")
    $(".analyseToast").toast("show");
    var object = {};
    var element_obj={};
    element_fd.forEach(function(value,key){
        element_obj[key]=value;
    });
    fd.forEach(function(value, key){
        object[key] = value;
    });
    object["config"]=element_obj
    object["currentsection"] = currentsection
    object['redirect_savedurl'] = location.hash.split('#')[1]
    object['Analysisname'] = $("#analyse_name_"+appID).val();
    var json = JSON.stringify(object);
    $.ajax({
	    url: location.origin+'/runCoverageGraphAnalysis/',
        data: json,
        processData: false,
        contentType: false,
        type: 'POST',
        success: function(data){
	        $('.runAnalysisToast').toast('show');
	        $('.analyseToast').toast('hide');

            closerp();
        },
        error: function (jqXHR, status, err) {
	        if (jqXHR.status == 401){
		        signout(true)
	        }else if(jqXHR.status == 400) {
                window.location.replace("https://auth.cyverse.org/cas5/logout?service=https://connect.bioviz.org/")
            }else if(jqXHR.status == 403){
                $('.analysisError').toast('show');
            }
	    },
            complete: function (jqXHR, status) {
        }
    });
}

function backtoMenu(){
 $("#appForm").css("display","none");
 $("#analysis").css("display","block");
}




