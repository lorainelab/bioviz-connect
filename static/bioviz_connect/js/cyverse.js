//cyverse.js
﻿tableData = [];
rpStatus = 0;
var rpStatus = 0;
var recordsLimit = 100;
var homePath = ""
var homePathforShare = "/iplant/home"
var homePathforCommunity = '/iplant/home/shared'
var url_search = 'searchFile/'
var main_Section = "home"
var page=0;
var url="";
var menuColor="active"
var minimumIGBVersion = "9.1.5"
$(document).ready(function (e) {
// disable left menu button
    $("#menuBtnHome").prop("disabled",true)
	getUserData("NAME","ASC")
    $("#searchRender").data("enableSearchRender", false)
    enableSearchkeyPress();
    $('[data-toggle="tooltip"]').tooltip();
//checking cookie warning status
    checkCookie();
    initialiseUser();
});

function initialiseUser(){
    $.ajax({
        type: "GET",
        dataType: "html",
        async: "true",
        url: location.origin+"/initializeUser",
            success: function (data, status, jqXHR) {
                // data.redirect contains the string URL to redirect to
        },
             error: function (jqXHR, status, err) {
                alert(err)
             },
             complete: function (jqXHR, status) {
             }
    });
}

function getPathJsonObject(){
    var object = {}
    location_path = location.hash.split('#')[1]
    object['redirect_savedurl'] = location_path
    object['currentSection'] = main_Section
    var json_path = JSON.stringify(object)
    return json_path
}

function closerp() {
//closes the right panel
    $(".right").css("display","none").animate();
    rpStatus = 0;
}

function redirectResetSectionState(path){
    if (path == ""){
        return true
    }
    $(".leftMenu").removeClass(menuColor);
    $("#searchText").prop("disabled",false);
    if(path.includes(homePathforCommunity)){
            $("#menuBtnCommunity").addClass(menuColor);
            $("#searchText").attr("placeholder","Search Community")
            main_Section="community"
            return true
    }else if(path.includes(homePath)){
            $("#menuBtnHome").addClass(menuColor);
            $("#searchText").attr("placeholder","Search Home")
            main_Section="home"
            return true
    }else if(path.includes(homePathforShare)){
            $("#menuBtnShared").addClass(menuColor);
            $("#searchText").attr("placeholder","Search Shared")
            main_Section="shared"
            return true
    }else if(path.includes("#/analyseslog")){
        $("#searchText").prop("disabled",true);
        $("#menuBtnAnalyses").addClass(menuColor);
        $("#searchText").attr("placeholder","")
        analysesHistory(recordsLimit, 0, true);
        return false
    }else if(path.includes('#/searchDirectory')){
        $("#searchRender").data("enableSearchRender", true)
        data = path.split('#/searchDirectory/')
        if (data.length > 1) {
            val = data[1]
        }
        else{
            val = ""
        }
        $("#searchText").val(val)
        if (main_Section == 'home'){
                        $("#menuBtnHome").addClass(menuColor);
        }else if(main_Section == 'shared'){
                        $("#menuBtnShared").addClass(menuColor);
        }else if(main_Section == 'community'){
                        $("#menuBtnCommunity").addClass(menuColor);
        }else if(main_Section == 'analyses'){
                    $("#menuBtnAnalyses").addClass(menuColor);
        }
        }
    return true
}

function updateSectionVariable(path){
    $(".leftMenu").removeClass(menuColor);
    $("#searchText").prop("disabled",false);
    if(path.includes(homePathforCommunity)){
            $("#menuBtnCommunity").addClass(menuColor);
            $("#searchText").attr("placeholder","Search Community")
            main_Section="community"
            return true
    }else if(path.includes(homePath)){
            $("#menuBtnHome").addClass(menuColor);
            $("#searchText").attr("placeholder","Search Home")
            main_Section="home"
            return true
    }else if(path.includes(homePathforShare)){
            $("#menuBtnShared").addClass(menuColor);
            $("#searchText").attr("placeholder","Search Shared")
            main_Section="shared"
            return true
    }else if(path.includes("#/analyseslog")){
        $("#searchText").prop("disabled",true);
        $("#menuBtnAnalyses").addClass(menuColor);
        $("#searchText").attr("placeholder","")
        analysesHistory(recordsLimit, 0, true);
        return false
    }
    return true
}

 window.onhashchange = function () {
    var path = location.hash;
    $("#searchRender #searchText").show();
	$("#searchRender .input-group-append").show();
	$(".breadcrumb").show();
    $("th").children(".fas").removeClass("fa-angle-down");
    $("th").children(".fas").addClass("fa-angle-up");
    updateSectionVariable(path)
    if(path=="#/analyseslog"){
        $("#menuBtnAnalyses").addClass(menuColor);
        analysesHistory(recordsLimit, 0, true);
    }else if(path.includes("searchDirectory")){
        $("#searchText").val(path.split("searchDirectory/")[1]);
        $("#searchRender").data("enableSearchRender",true)
        getFileList("", recordsLimit, offset,'NAME', 'ASC',main_Section, true)
    }else{
        $("#searchRender").data("enableSearchRender",false)
        pathstr = path.split('#')
        if(pathstr.length >2){
  		    filepath = pathstr.slice(1).join("#");
        }
        else{
  	        filepath = pathstr[1];
        }
        getFileList(filepath, recordsLimit, 0,'NAME', 'ASC',main_Section, true)
    }
 }
function signout(isRedirect_SavedPage){
    var object = {};
    object['isRedirect_SavedPage'] = isRedirect_SavedPage
    var json = JSON.stringify(object);
	$.ajax({
        method: "POST",
        dataType: "html",
        data: json,
        async: "true",
		url: location.origin+"/Signout/",
	    success: function (data, status, jqXHR) {
            if (isRedirect_SavedPage) {
                url_root = "https://auth.cyverse.org/cas5/logout?service="+data
                window.location.replace(url_root);
            }
            else{
                window.location.replace("https://auth.cyverse.org/cas5/logout?service=https://www.bioviz.org/connect.html");
            }
        },
        error: function (jqXHR, status, err) {
            alert("Sorry! Ran into an error while signing out");
		},
        	complete: function (jqXHR, status) {
        }
    });
}
function checkCookie(){
//checking if cookie warning should be displayed or not
  $.ajax({
		method: "GET",
        dataType: "html",
        async: "true",
		url: location.origin+"/gcookie",
	    success: function (data, status, jqXHR) {
//gcookie returns True if the cookie for the website was NEVER set in the client browser.
	        if(data=="True"){
	           $('#cookieModal').modal('show');
	        }
	    },
	    error:function(jqXHR, status, err){
	        alert(err);
	    }
    });

}
function setCookie(){
//sets the date of last cookie warning message in the cookie of the website in the client browser.
  $.ajax({
		method: "GET",
        dataType: "html",
        async: "true",
		url: location.origin+"/scookie"
    });
}

function getUserData(sortcol,sortdir){
//Fetching user metadata
	$.ajax({
		method: "POST",
        dataType: "json",
        async: "true",
        data:getPathJsonObject(),
		url: location.origin+"/getRootPathUser/",
	    success: function (data, status, jqXHR) {
	    //setting username
	        var username=data.roots[0].label
	        $("#menuBtnHome").prop("disabled",false)
	        $("#menuBtnHome").addClass(menuColor);
	        $("#usernamedpdn").append(username)
        //setting home, community and shared folder paths
            homePath = data.roots[0].path
            homePathforCommunity = data.roots[1].path
            homePathforShare = data.roots[2].path
            fldrpth=location.hash;
	        if (fldrpth != "") {
	            if (fldrpth.includes("?currentSection=")){
	                 data = fldrpth.split("?currentSection=")
	                 //sets main_section variable with section in the url
                     main_Section = data[1]
                     //sets folder path
                     fldrpth = data[0]
                     isredirectgetFileList = redirectResetSectionState(fldrpth)
                     location.hash = fldrpth
                }else{
                 /*checks if the folder path belongs in the home, community or shared sections
                 and sets main_section variable correspondingly*/
	                action = updateSectionVariable(fldrpth)
                    if (action) {
                 //gets file directory of based on the url location in the browser
                        getFileList(fldrpth.split("#")[1], recordsLimit, offset, 'NAME', 'ASC', main_Section, true)
                    }
                }
            }else{
            //if folderpath is empty go to homepath
               location.hash=homePath;
            }

        },
        error: function (jqXHR, status, err) {
            loadingDisplay(".middleLoad",false)
            if(jqXHR.status == 401){
                signout(true)
            }
            else if(jqXHR.status == 400) {
                window.location.replace("https://auth.cyverse.org/cas5/logout?service=https://connect.bioviz.org/")
            }
            else{
           	    alert("Sorry! Unable to retrieve User data. Please try again or contact admin.")
           	}

		},
        complete: function (jqXHR, status) {}
	});
}
function sortTable(sortcol,element){
//checks if arrow icon next to the column header is up
    if($(element).children(".fas").hasClass("fa-angle-up")){
        $("th").children(".fas").addClass("fa-angle-up");
        //change the file list to decending order
        getFileList(location.hash.split("#")[1], recordsLimit, offset, sortcol, "DESC",main_Section, true)
        //change arrow icon to down
        $(element).children(".fas").removeClass("fa-angle-up")
        $(element).children(".fas").addClass("fa-angle-down")
    }else if($(element).children(".fas").hasClass("fa-angle-down")){
    //checks if arrow icon next to the column header is down
        $("th").children(".fas").addClass("fa-angle-up");
        //change the file list to ascending order
        getFileList(location.hash.split("#")[1], recordsLimit, offset, sortcol, "ASC",main_Section, true)
        //change arrow icon to up
        $(element).children(".fas").removeClass("fa-angle-down")
        $(element).children(".fas").addClass("fa-angle-up")
    }

}
function renderSectionGetFiles(section,sortcol,sortdir,fromsearch){

//change all column header sort icons to up
     if($("th").children(".fas").hasClass("fa-angle-down")){
            $("th").children(".fas").addClass("fa-angle-up");
     }

    var searchKey =$("#searchText").val();
    var path = location.hash;
//checks if request is from search
   	if (fromsearch){
   	//checks section and adds section to main_section variable
        updateSectionVariable(path)
        $("#searchRender").data("enableSearchRender", true)
    }
    else{
        //remove section button highlight
        $(".leftMenu").removeClass(menuColor);
        main_Section = section
        $("#searchRender").data("enableSearchRender", false)
    }
    offset=0;
 //setting default state of search input as NOT disabled
    $("#searchText").prop("disabled",false);
    if (main_Section=="home") {
        $("#menuBtnHome").addClass(menuColor);
        $("#searchText").attr("placeholder","Search Home")
        fldrpth=homePath;
        fromsearch? location.hash = "/searchDirectory/"+searchKey:location.hash = fldrpth
    }
    else if (main_Section == "community"){
        $("#menuBtnCommunity").addClass(menuColor);
        $("#searchText").attr("placeholder","Search Community")
        fldrpth=homePathforCommunity;
        fromsearch? location.hash = "/searchDirectory/"+searchKey:location.hash = fldrpth
    }
    else if(main_Section == "shared"){
        $("#menuBtnShared").addClass(menuColor);
        $("#searchText").attr("placeholder","Search Shared")
        fldrpth=homePathforShare;
        fromsearch? location.hash = "/searchDirectory/"+searchKey:location.hash = fldrpth
    }
    else if(main_Section == "analyses"){
    //if analyses section is selected search is disabled
        $("#searchText").prop("disabled",true);
        $("#menuBtnAnalyses").addClass(menuColor);
        $("#searchText").attr("placeholder","")
        location.hash="/analyseslog"
    }
}
function disableSearchRender(){
        $("#searchRender").data("enableSearchRender", false)
}
function enableSearchkeyPress(){
  	$( "#searchText" ).keypress(function(event) {
        	if (event.which == 13){
            		renderSectionGetFiles("","NAME","ASC",true) // on Enter press
        	}
    	});
}

function getFileList(url, limit, offset, sortcol, sortdir, section, defaultaction) {
        $("#analysesHist").hide();
        $("#fileFolder").show();
        page_dir=sortdir;
        page_col=sortcol;
        closerp();
        var serviceurl = ""
        var renderSearch = $("#searchRender").data("enableSearchRender")
        var baseurl=""
        var func=""
        loadingDisplay(".middleLoad",true)
        if(section=="community"){
            baseurl="/getCommunityData/"
			func="getCommunityData"
        }else if(section=="shared"){
            baseurl="/renderSharedData/"
            func="getShareFileList"
        }
        else{
            baseurl="/middlepanel/"
          	func="getFileList"
        }
        if(renderSearch){
		    var label =  $("#searchText").val()
            var baseurl="/searchFile/"
            serviceurl = location.origin+baseurl
        }else {
            $("#searchText").val("")
            $("#searchRender").data("enableSearchRender", false)
            searchBack=url
            serviceurl = location.origin+baseurl
	    }
        var object = {}
        object['url'] = url
        object['limit'] = recordsLimit
        object['offset'] = offset
        object['sortcol'] = sortcol
        object['sortdir'] = sortdir
        location_path = location.hash.split('#')[1]
        object['redirect_savedurl'] = location_path

         if (object['redirect_savedurl'].includes('searchDirectory')){
            object['current_section_search'] = main_Section
        }

        if (renderSearch){
            object['label'] = label
            object['type'] = section
        }
        var json = JSON.stringify(object)
	$.ajax({
		type: "POST",
		url: serviceurl,
        dataType: "json",
        data:json,
	        success: function (data, status, jqXHR) {
               	$("#page-selection").data("currentPath",url)
               	$("#page-selection").data("currentSection", section)
               	newUploadBtnFn(data.fileSystemInfo.permission)
               	setupPagination(data, false, false, defaultaction)
	        	interpretData(data, func)
		       	buildBreadCrumbs(data, func)
	        	loadingDisplay(".middleLoad",false)
        },
           	error: function (jqXHR, status, err) {
		        if (jqXHR.status == 401){
                    signout(true)
                 }
                else if(jqXHR.status == 400) {
                window.location.replace("https://auth.cyverse.org/cas5/logout?service=https://connect.bioviz.org/")
                }
		        else {
                    alert("Sorry! Unable to retrieve files. Please try again or contact admin")
                }
           	    loadingDisplay(".middleLoad",false)
	    },
           	complete: function (jqXHR, status) {
        }
	});
}
function newUploadBtnFn(check){
	if(check != "own" && check != "write"){
		$('#newUploadBtn').prop('disabled',true);
		$('#newUploadBtn').removeClass("btn-outline-info");
		$('#newUploadBtn').addClass("btn-outline-secondary");
	}else{
		$('#newUploadBtn').prop('disabled',false);
		$('#newUploadBtn').removeClass("btn-outline-secondary");
		$('#newUploadBtn').addClass("btn-outline-info");
	}

}
//populating tables
var currentFiles = []
var currentFolders = []
function populateTable(tableData, type) {
        var menuButton ='<button type="button" class="btn popMenu"><i class="fas fa-ellipsis-v"></i></button>'
        if (type == "folders") {
                for (var data in tableData) {
                        var row = "<tr id='" + tableData[data].id + "' class='folder '>"+
                        "<td class='priority-2'></td>"+
                        "<td title='" + tableData[data].label + "' class='priority-1 align-middle' ><i class='fas fa-folder mr-3'></i>" + tableData[data].label + "</td>"+
                        "<td class='priority-3 text-secondary small align-middle'>-</td>"+
                        "<td class='priority-4 text-secondary small align-middle'  title='" + tableData[data].dateModified + "'>" + dateFormatChange(tableData[data].dateModified) + "</td></tr>"

                        $('#fileFolder').children('tbody').append(row)
                        $("#" + tableData[data].id).data("info", tableData[data])
                        currentFolders.push(tableData[data].label)
                }
                $("#fileFolder").data("currentFolders",currentFolders)
        } else if (type = "files") {
        //setting view in IGB status
                for (var data in tableData) {
//                        console.log(tableData[data].label)
//                        console.log("isViewable: "+tableData[data].isViewable+"\tpermission: "+tableData[data].permission+"\tisPublic: "+tableData[data].isPublic)
                        var permission = ""
                        if(tableData[data].isViewable == true) {
                                //if the file is not public checking if the user owns the file
	                        if (tableData[data].isPublic == true)
                                permission = "public"
                                if(tableData[data].permission == 'own')
                                    permission = "own"
                                else
                                    permission = "not own"
							var viewInIgbButton = '<button id="view_'+tableData[data].id+'" class="btn btn-success mr-1 w-100 h-100" onclick = "viewInIgb(\'' + tableData[data].path + '\',\'' + tableData[data].id + '\',\'' + permission + '\' , this)">View in IGB</button>\n';
                        }else{
                            var viewInIgbButton = '';
                        }
//                        console.log(viewInIgbButton)
                        var row = "<tr  id='" + tableData[data].id + "' class='file'>"+
                        "<td class='priority-2'>"+ viewInIgbButton  + "</td>"+
                        "<td title='" + tableData[data].label + "' class='priority-1 align-middle'><i class='far fa-file mr-3'></i>" + tableData[data].label + "</td>"+
                        "<td class='priority-3 text-secondary small align-middle'>" + editSize(tableData[data].fileSize) + "</td>"+
                        "<td class='priority-4 text-secondary small align-middle' title='" + tableData[data].dateModified + "'>" + dateFormatChange(tableData[data].dateModified)+ "</td></tr>"

                        $('#fileFolder').children('tbody').append(row);
                        $("#" + tableData[data].id).data("info", tableData[data]);
                        currentFiles.push(tableData[data].label)
                }
                 $("#fileFolder").data("currentFiles",currentFiles)

        }
}
function editSize(size){
    var value = size.split(" ")[0]
    var unit = size.split(" ")[1]
    if(value.includes(".")){
        if(value.split(".")[0].length == 3)
            return Math.ceil(value) +" "+unit
        if(value.split(".")[0].length ==2)
            return Math.round(value*10)/10 +" "+unit
        if(value.split(".")[0].length ==1)
            return Math.round(value*100)/100 +" "+unit
        return value.slice(0,4) +" "+unit
   }else{
        return value.slice(0,3) +" "+unit
   }
   return value +" "+unit;
}
function dateFormatChange(dateTimeStr){
	 var months = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
	var dateStr = dateTimeStr.split(" ")[0].replaceAll("-","/")
//	var timeStr = dateTimeStr.split(" ")[1]
	var date = new Date(dateStr)
    var dateFormatted = months[date.getMonth()] +" "+date.getDate()+", "+date.getFullYear();
	return dateFormatted
}
//loading spiral turned on or off
function loadingDisplay(loading,bool){
    	if(bool){
        	$(loading).css("display","block")
    	}else{
        	$(loading).css("display","none")
    	}
}
function redirectAnalysesOutputFolder(outputfolderpath){

    location.hash= outputfolderpath
}
function analysesHistory(recordslimit, offset, defaulaction){
//hiding file directory;showing annlyses section;empyting breadcrumbs section and replacing with analyses icon
    $("#fileFolder").hide();
	$("#analysesHist").show();
	$(".breadcrumb").html('');
	$("#searchRender #searchText").hide();
	$("#searchRender .input-group-append").hide();
	$(".breadcrumb").hide();
//closing rightpanel; loading spiral active
	closerp();
	loadingDisplay(".middleLoad",true)

    var object = {}
    object['redirect_savedurl'] = location.hash.split('#')[1]
    object['limit'] = recordsLimit
    object['offset'] = offset
    var json= JSON.stringify(object)

	$.ajax({
		method: "POST",
		dataType: "json",
		async: "true",
        data: json,
		url: location.origin+"/getAnalyze/",
		success: function (data, status, jqXHR) {
			$('#analysesHist').children('tbody').html("")
			$("#page-selection").data("currentPath", location.origin+"/getAnalyze/")
            $("#page-selection").data("currentSection", "Analyses")
            setupPagination(data, false, false, defaulaction)
			analyseData=data["analyses"];
			var statusCol="";
			//populating analyses jobs and statuses
			for(var obj in analyseData){
				if(analyseData[obj].status==("Completed")){
					statusCol = "table-success";
				}else if(analyseData[obj].status=="Failed"){
					statusCol = "table-danger";
				}else if(analyseData[obj].status=="Running"){
					statusCol = "table-warning";
				}
				var row = "<tr><td class='analysesName' onclick=redirectAnalysesOutputFolder('"+(uriEncodeFileName(analyseData[obj].resultfolderid, 1))+"')>" + analyseData[obj].name + "</td><td>" +analyseData[obj].app_name + "</td><td>" +analyseData[obj].startdate + "</td><td>" +analyseData[obj].enddate + "</td><td class="+statusCol+" table-bordered>" +analyseData[obj].status + "</td></tr>"
            			$('#analysesHist').children('tbody').append(row)
			}
			location.hash= "/analyseslog"
			loadingDisplay(".middleLoad",false)
		},
		error: function (jqXHR, status, err) {
		    if(jqXHR.status == 401){
                signout(true)
            }else if(jqXHR.status == 400) {
                window.location.replace("https://auth.cyverse.org/cas5/logout?service=https://connect.bioviz.org/")
            }
			loadingDisplay(".middleLoad",false)
		},
		complete: function (jqXHR, status) {
		}
	});
}

function manageLinkOperations_frompopup(){
    result = manageLinkOperations('create', true)
    $('#makepublic').modal('toggle')
}
function compareVersion(v1, v2) {
    if (typeof v1 !== 'string') return false;
    if (typeof v2 !== 'string') return false;
    v1 = v1.split('.');
    v2 = v2.split('.');
    const k = Math.min(v1.length, v2.length);
    for (let i = 0; i < k; ++ i) {
        v1[i] = parseInt(v1[i], 10);
        v2[i] = parseInt(v2[i], 10);
        if (v1[i] > v2[i]) return 1;
        if (v1[i] < v2[i]) return -1;
    }
    return v1.length == v2.length ? 0: (v1.length < v2.length ? -1 : 1);
}
function viewInIgb(path, id, perm,element) {
//onclick of View In IGB button
	element.innerHTML = "<i class='fas fa-sync fa-spin'></i>"
// Check is public
    isPublic_Search(path, id, perm ,element)
}

function isPublic_Search(path, id,perm,element){
    var object = {};
    object['path'] = path
    object['redirect_savedurl'] = location.hash.split('#')[1]
    var json = JSON.stringify(object);

	urlLink = location.origin+"/checkFilePermission/"

	if(perm == "not own")
		urlLink=location.origin+"/checkFilePermission_Search/"

	//"Checking if igb is running..." toast appears
	$('.igbRunCheck').toast('show');
    $.ajax({
        method: "POST",
        data: json,
        processData: false,
        contentType: false,
       	async: "true",
	    url: urlLink,
	    success: function (data, status, jqXHR) {
        	is_public = false
        	var userPerm=data.paths[0]["user-permissions"]
            for(var i=0;i<userPerm.length;i++) {
				if (userPerm.length != 0 && (userPerm[i].user == "anonymous" || userPerm[i].user == "anonymous#iplant") && userPerm[i].permission == "read") {
					is_public = true
				}
			}
			if (!is_public){
				if (perm == 'own'){
					element.innerHTML = "View in IGB"
                    $('#makepublic').modal('show');
				}
				else{
					$('.fileNotPublicNotOwner').toast('show');
					element.innerHTML = "View in IGB"
				}
			}else{
				checkAndOpenInIGB(element,path,id)
			}
			loadingDisplay(".rightLoad",false)
		},
		error: function (jqXHR, status, err) {
            if (jqXHR.status == 401){
				signout(true)
	        }else if(jqXHR.responseText == "ERR_NOT_OWNER"){
	            $('.fileNotPublicNotOwner').toast('show');
	        }else{
                alert("Sorry! An error occured while running the search. Please try again or contact admin")
            }
            element.innerHTML = "View in IGB"
            loadingDisplay(".rightLoad",false)
		},
        complete: function (jqXHR, status) {}
    });
}
function checkAndOpenInIGB(element,path,id){
	var statusCheckUrl = 'http://127.0.0.1:7085/igbStatusCheck';
	var xhr = createCORSRequest('GET', statusCheckUrl);
	if (!xhr) {return;}
	xhr.onload = function () {
		if (xhr.status == 200){
			var response = xhr.response
            if (response.includes('igbVersion')){
	            var igbVersion = response.split('=')[1]
                if (compareVersion(igbVersion, minimumIGBVersion) < 0){
					$('#igbUpdateWarning').modal('show');
                }
            }else{
				$('#igbUpdateWarning').modal('show');
            }
			igbIsRunning = true;
			getMetaData(path, id);
			$('.igbRunCheck').toast('hide');
			element.innerHTML = "View in IGB"
			$('.igbRun').toast('show');
			}else {
			igbIsRunning = false;
			element.innerHTML = "View in IGB"
		}
	};
	xhr.onerror = function () {
		igbIsRunning = false;
		$('#igbWarning').modal('show');
			element.innerHTML = "View in IGB"
		};
		xhr.send();
	}
function uriEncodeFileName(server_url, numberofencodings)
{
    var words = server_url.split('/')

    index = 0
    wordslength = words.length

    words.forEach(function (e) {
                    if (e=='https:')
                    {
                        index = index + 1
                        return;
                    }
                    if (numberofencodings == 2){
                        encodedfilename = encodeURIComponent(encodeURIComponent(e))
                    }
                    else{
                        encodedfilename = encodeURIComponent(e)
                    }
                    words[index] = encodedfilename
                    index = index + 1
               });
    encodedfilepath = words.join('/')
    return encodedfilepath
}
function makeAndOpenIgbUrl(filename,server_url,version,foreground,background,trackname) {
    if(version=="Version..."||version==""){version="?"}
    var igb_url="http://127.0.0.1:7085/IGBControl?version="+version+"&cyverse_data=true&sym_col_0="+foreground+"&sym_bg_0="+background+"&sym_name_0="+encodeURIComponent(trackname);
    server_url =  uriEncodeFileName(server_url, 2)
    igb_url=igb_url+"&query_url="+server_url+"&feature_url_0="+server_url+"&sym_method_0="+server_url+"&loadresidues=false&server_url=cyverse";
    var xhr = createCORSRequest('GET', igb_url);
    if (!xhr) {
        return;
    }
    console.log("Opening IGB Url: " + igb_url);
    xhr.send();
}
function addCoordinatesForGeneId(geneId) {
    var gene_url = location.origin+"/cgi-bin/geneIdLookup.py?gene_id="+geneId;
    var xhr = createCORSRequest('GET', gene_url);
    if (!xhr) {
       console.log("Could not form request: "+ gene_url);
    }
    xhr.onload = function() {
        if (xhr.status=200){
            gene_coords = JSON.parse(this.response);
            igb_params["start"]=parseInt(gene_coords["start"])-500;
            igb_params["end"]=parseInt(gene_coords["end"])+500;
            igb_params["seqid"]=gene_coords["seqid"];
            makeAndOpenIgbUrl();
        }
    };
    xhr.onerror = function(err) {
        console.log(err);
    };
    xhr.send();
}
// Create and return an XHR object.
function createCORSRequest(method, url) {
    var xhr = new XMLHttpRequest();
    if ("withCredentials" in xhr) {
        // XHR for Chrome/Firefox/Opera/Safari.
        xhr.open(method, url, true);
    } else if (typeof XDomainRequest != "undefined") {
        // XDomainRequest for IE.
        xhr = new XDomainRequest();
        xhr.open(method, url);
    } else {
        // CORS not supported.
        xhr = null;
    }
    return xhr;
}

function getMetaData(path,fileId){
        var metaData=""
        var filename=path.split("/")[path.split("/").length-1]

        var object = {};
        object['fileId'] = fileId
        object['redirect_savedurl'] = location.hash.split('#')[1]
        var json = JSON.stringify(object);

        $.ajax({
            method: "POST",
            dataType: "json",
            data: json,
            async: "true",
            url: location.origin+"/retrieveMetaData/",
            success: function (data, status, jqXHR) {
                metaData=data;
                var version = "";
                var foreground = "";
                var background = "";
                var trackname = "";
                if(metaData != null && metaData["avus"].length>0)
                {
                    var jsonData = metaData["avus"]
                    for(item in jsonData){
                        switch(jsonData[item]["attr"]){
                            case "Genome":
                               version = jsonData[item]["value"].split(";")[1]
                               break;
                            case "foreground":
                                foreground = jsonData[item]["value"].toUpperCase().replace("#","0x")
                                break;
                            case "background":
                                background = jsonData[item]["value"].toUpperCase().replace("#","0x")
                                break;
                            case "trackName":
                                trackname = jsonData[item]["value"];
                                break;
                            default:
                                break;
                        }

                    }
                }
                if(trackname=="")
                    trackname = filename;
                var server_url = "https://data.cyverse.org/dav-anon"+path;
                makeAndOpenIgbUrl(filename,server_url,version,foreground,background,trackname); // no need to query web service

        },
        error: function (jqXHR, status, err) {
                if(jqXHR.status == 401){
                signout(true)
                }else if(jqXHR.status == 400) {
                window.location.replace("https://auth.cyverse.org/cas5/logout?service=https://connect.bioviz.org/")
            } else{
                    alert("Sorry! Unable to retrieve Metadata for this file. Please try again or contact admin.")
                }
            },
        complete: function (jqXHR, status) {
        }
        });
}
function installerDownload(){
    var url = ""
    if (navigator.appVersion.indexOf("Win")!=-1) {
        if(navigator.userAgent.indexOf("Win64") != -1 ){
            url = "https://bioviz.org/igb/releases/current/IGB_windows-x64_current.exe"
        }else{
            url = "https://bioviz.org/igb/releases/current/IGB_windows_current.exe"
        }
    };
    if (navigator.appVersion.indexOf("Mac")!=-1) url = "https://bioviz.org/igb/releases/current/IGB_macos_current.dmg";
    if (navigator.appVersion.indexOf("Linux")!=-1) url= "https://bioviz.org/igb/releases/current/IGB_unix_current.sh";
    var link = document.createElement("a");
    link.href = url;
    link.click();
    $('#igbWarning').modal('hide');
}
